#!/bin/bash
# Install Ansible
sudo easy_install pip
sudo pip install ansible

# Install the Mac command-line tooling
xcode-select --install

# Install git - may be installed by the xcode-select
# Only Curl is present - and no Git

git clone https://bitbucket.org/todd_morgan/mac-dev-playbook.git

cd mac-dev-playbook
ansible-galaxy install -r requirements.yml

ansible-playbook main.yml -i inventory -K
# Add in the Mac Docker client